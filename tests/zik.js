const Readable = require('stream').Readable;
const inherits = require('inherits');
const Throttle = require('throttle');
const _2PI = Math.PI * 2.0;

var Frequencies = [32.70,34.65,36.71,38.89,41.20,43.65,46.25,49.00,51.91,55.00,58.27,61.74];



var LowerPassFilter = (function()
{
	function LowerPassFilter()
	{
		this.level = 0.3;
		this.backup = 0;
	 	this.wet = 0.5;

		this.input = null;
	}
	LowerPassFilter.prototype.process = function(t)
	{
		if (this.input)
		{
			var input_value = this.input.process(t);
			var result = input_value + this.backup * this.level;
			this.backup = result;
			return result * this.wet + input_value * (1.0 - this.wet);
		}
	}
	return LowerPassFilter;
})();

var UpperPassFilter = (function()
{
	function UpperPassFilter()
	{
		this.level = 0.3;
		this.backup = 0;
	 	this.wet = 0.5;

		this.input = null;
	}
	UpperPassFilter.prototype.process = function(t)
	{
		if (this.input)
		{
			var input_value = this.input.process(t);
			var result = input_value + this.backup * this.level;
			this.backup =  input_value - result;
			return result * this.wet + input_value * (1.0 - this.wet);
		}
	}
	return UpperPassFilter;
})();

var ToneGenerator = (function()
{
	function ToneGenerator()
	{
		this.frequency = 440.0;	
		this.amplitude = 1.0;
	}
	ToneGenerator.prototype.process = function(t)
	{
		var phase = t * this.frequency;
	    return Math.sin(_2PI * phase)  * this.amplitude ;
	}
	return ToneGenerator;
})();

var FMBassBkp = (function()
{
	function FMBassBkp()
	{
		this.frequency = 55.0;
		this.amplitude = 1.0;
		this.fb = 0.0;
		this.amp0 = 0.75;
		this.amp1 = 1.0;
	}
	FMBassBkp.prototype.process = function(t)
	{
		var phase0 = this.frequency * 0.5 * t + this.fb * 0.097 ;
		var x0 = Math.sin(_2PI * phase0) * this.amp0;
		var phase1 = this.frequency * t + x0;
		var x1 = Math.sin(_2PI * phase1)*this.amp1;
		this.fb = x0 ;
		return x1 * this.amplitude;	
	}
	return FMBassBkp;
})();

var FMBass = (function()
{
	function FMBass()
	{
		this.frequency = 55.0;
		this.amplitude = 1.0;
		this.amp0 = 0.75;
		this.amp1 = 1.0;
		this.harmonization = 0.99;
	}
	FMBass.prototype.process = function(t)
	{
		var phase0 = this.frequency * this.harmonization * t;
		var x0 = Math.sin(_2PI * phase0) * this.amp0;
		var phase1 = this.frequency * t + x0;
		var x1 = Math.sin(_2PI * phase1) * this.amp1;
		return x1 * this.amplitude;	
	}
	return FMBass;
})();

var FMSynthetizer = (function()
{
	function FMSynthetizer()
	{
		this.frequency = 55.0;
		this.amplitude = 1.0;
		this.amp0 = 0.75;
		this.amp1 = 1.0;
		this.harmonization = 0.99;
	}
	FMSynthetizer.prototype.process = function(t)
	{
		var phase0 = this.frequency * this.harmonization * t;
		var x0 = Math.sin(_2PI * phase0) * this.amp0;
		var phase1 = this.frequency * t + x0;
		var x1 = Math.sin(_2PI * phase1) * this.amp1;
		return x1 * this.amplitude;	
	}
	return FMSynthetizer;
})();


var ZikStreamer = (function()
{
	const buffer_size = 4096; 
	function ZikStreamer()
	{
		Readable.call(this);
		
		this.generator = (function()
		{ 
			var tone = new ToneGenerator();
			return function(t) { return tone.process(t);}
		})();
		
		this.rate = 44100;
		this.dt = 1.0/this.rate;
		this.time = 0.0;		 
	}

	inherits(ZikStreamer,Readable);

	convert = function (x)
	{
	    if (isNaN(x)) return 0;        
	    var cx = x;
	    if (cx > 1.0) { cx = 1.0;}
	    else if (cx < -1.0) {cx = -1.0;}
	    return Math.floor(cx * 32767);
	}

	ZikStreamer.prototype._read = function(bytes)
	{
		if (!bytes)
		{
			bytes = this.buffer_size; 
		}
		var buffer = new Buffer(bytes);
		for (var index = 0; index < bytes; index += 2)
		{
			var value = convert(this.generator(this.time));
			buffer.writeInt16LE(value,index);
			this.time = this.time + this.dt; 
		}
		this.push(buffer);
	}
	ZikStreamer.prototype.setRate = function(rate)
	{
		this.rate = rate;
		this.dt = 1.0/this.rate;
	}

	return ZikStreamer;
})();

var zik = new ZikStreamer();
var throttle = new Throttle(zik.rate*8);

zik.generator = (function()
{
	var up = true;
	var frequency_index = 0;
	
	var tone = new FMBass();
	tone.frequency = Frequencies[5];
	tone.amplitude = 0.5;
	
	var filter = new LowerPassFilter();
	filter.level = 0.9;
	filter.wet = 0.0;
	filter.input = tone;
	
	var note = new Envelope();
	note.a = 1;
	note.s = 3;
	note.ti = 0.0;
	note.input = filter;


	return function(t)
	{
		if ( t > (note.ti + note.length() ) )
		{					
			if (frequency_index == 0)
			{
				up = true;
			}
			if (frequency_index == 5)
			{
				up = false;
			}
			if (up)
			{
				frequency_index++;
			}
			else
			{
				frequency_index--;
			}
			var frequency = Frequencies[frequency_index];
			tone.frequency = frequency; 
			note.ti += note.length();
		}
		return note.process(t);
	}
})();

zik.pipe(throttle).pipe(process.stdout);