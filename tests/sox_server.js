const Throttle = require('throttle');
const Express = require('express');
const inherits = require('inherits');
const Readable = require('stream').Readable;
const Spawn = require('child_process').spawn;

close_handle = function(code) { console.log('close'); }
error_handle = function(data) { console.log('error '+data); }


sound = function (t) 
{
	var f = 441.0 * (Math.sin(2 * Math.PI * t * 5.0) * 0.01 + 1.0);
    return Math.sin(2 * Math.PI * t * f) ;
};

convert = function (n) 
{
    if (isNaN(n)) return 0;
    var b = Math.pow(2, 15);
    return n > 0
        ? Math.min(b - 1, Math.floor((b * n) - 1))
        : Math.max(-b, Math.ceil((b * n) - 1))
    ;
};

var SoxStreamer = function()
{
	Readable.call(this);
}

inherits(SoxStreamer,Readable);

var time_duration = 3.0;
var rate = 44100;
var buffer_size = 4096;
var buffer_duration = buffer_size / (rate * 2);
var time_number = Math.floor(time_duration / buffer_duration);
var time = 0.0;
var dt = 1.0/rate;

SoxStreamer.prototype._read = function read(bytes) 
{
	var self = this;
	if (time > time_duration)
	{
		process.nextTick(function() { self.push(null); })
	}
	else
	{
		if (!bytes)
		{
			bytes = buffer_size; 
		}
		var buffer = new Buffer(buffer_size);
		for (var index = 0; index < buffer_size; index+=2)
		{
			var value = convert(sound(time));
			buffer.writeInt16LE(value,index);
			time = time + dt; 
		}
		process.nextTick(function() { self.push(buffer);});
	}
}


var app = Express();
app.get('/beep', function (req, res) {
	var streamer  = new SoxStreamer();
	//var throttle =  new Throttle(128000 / 8 );
  	//res.setHeader('Accept-Ranges', 'bytes');
  	//res.setHeader('Content-Range', 'bytes');
    
    res.set({
      'Content-Type': 'audio/mpeg',
      'Connection':'close',
      'Transfer-Encoding': 'identity'
    });
    	
  	//res.setHeader('Content-Type', 'audio/mpeg');	
	var sox = Spawn('sox',['-r','44100','-c','1','-t','s16','-','-C','128', '-t', 'mp3', '-']);
	sox.on('close', close_handle);
	sox.stderr.on('data', error_handle);
	sox.stdin.on('error',error_handle);
	streamer.pipe(sox.stdin);
	sox.stdout.pipe(res);
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});