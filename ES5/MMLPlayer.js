const inherits = require('inherits');
var MMLIterator = require('mml-iterator');
var IProcessor = require('./IProcessor.js');
var Envelope = require('./Envelope.js');
var Oscillator = require('./Oscillator.js'); 
var NoteTranslator = require('./NoteTranslator.js');
var MMLPlayer = (function(){	
	function MMLPlayer()
	{
		IProcessor.call(this);
		this.mml_score = "";
		this.mml_score_iterator = null;
		this.current_note = null;
		this.voice = null;
		this.envelope = new Envelope();
		this.starting_time = 0.0;
		this.is_restarting = false;
	}

	inherits(MMLPlayer,IProcessor);

	MMLPlayer.prototype.setScore = function(score_)
	{
		this.score = score_;
		this.resetScore();
	}

	MMLPlayer.prototype.restart = function()
	{
		this.is_restarting = true;
	}

	MMLPlayer.prototype.resetScore = function(t)
	{
		this.mml_score_iterator = new MMLIterator(this.score);
		this.current_note = null;
		this.next();
	}

	MMLPlayer.prototype.endOfScore = function()
	{
		if (this.current_note.done == true 
			|| this.current_note.value.type == 'end')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	MMLPlayer.prototype.next = function(t)
	{
		if (this.current_note == null || !this.endOfScore()) 
		{
			this.current_note = this.mml_score_iterator.next();
			if (this.endOfScore())	
			{
				this.voice.enabled = false;
			}
			else if (this.current_note.value.type == 'note')
			{
				this.voice.enabled = true;
				var frequency = NoteTranslator.ToFrequency(this.current_note.value.noteNumber); 
				this.voice.setFrequency(frequency);
				var start_time =   this.current_note.value.time + this.starting_time;
				this.envelope.setStartTime(start_time);
				this.envelope.setLength(this.current_note.value.duration);
			}
		}
	}

	MMLPlayer.prototype.process = function(t)
	{
		if (this.is_restarting)
		{
			this.starting_time = t;
			this.is_restarting = false;
			this.resetScore();
		}		
		if (this.endOfScore() == false && this.envelope.hasEnded(t))
		{
			this.next();
		}
		return this.voice.process(t) * this.envelope.process(t); 
	}

	return MMLPlayer;
})();

module.exports = MMLPlayer;