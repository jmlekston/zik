const Readable = require('stream').Readable;
const inherits = require('inherits');

var SoundEngine = (function()
{
	const buffer_size = 4096; 
	function SoundEngine()
	{
		Readable.call(this);
		this.generator = function(t)
		{
			return Math.sin(Math.PI * 2.0 * t * 440.0);
		}		
		this.rate = 44100;
		this.dt = 1.0/this.rate;
		this.time = 0.0;		 
	}

	inherits(SoundEngine,Readable);

	convert = function (x)
	{
	    if (isNaN(x)) return 0;        
	    var cx = x;
	    if (cx > 1.0) { cx = 1.0;}
	    else if (cx < -1.0) {cx = -1.0;}
	    return Math.floor(cx * 32767);
	}

	SoundEngine.prototype._read = function(bytes)
	{
		if (!bytes)
		{
			bytes = this.buffer_size; 
		}
		var buffer = new Buffer(bytes);
		for (var index = 0; index < bytes; index += 2)
		{
			var value = convert(this.generator(this.time));
			buffer.writeInt16LE(value,index);
			this.time = this.time + this.dt; 
		}
		this.push(buffer);
	}
	SoundEngine.prototype.setRate = function(rate)
	{
		this.rate = rate;
		this.dt = 1.0/this.rate;
	}

	return SoundEngine;
})();

module.exports = SoundEngine;