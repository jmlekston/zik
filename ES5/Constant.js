var IProcessor = require('./IProcessor.js');
const inherits = require('inherits');

var Constant = (function()
{
	function Constant(value_) 
	{
		IProcessor.call(this);
		this.value = value_;
	}
	inherits(Constant, IProcessor);
	Constant.prototype.process = function(t)
	{
		return this.value;
	}
	return Constant;
})();

module.exports = Constant;