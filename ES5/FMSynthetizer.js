var ISoundGenerator = require('./ISoundGenerator.js');
var Oscillator = require('./Oscillator.js');
var Constant = require('./Constant.js');
const inherits = require('inherits');

const _2PI = Math.PI * 2.0;

var FMSynthetizer = (function()
{

	function FMSynthetizer(operator_number)
	{
		ISoundGenerator.call(this);
		this.operators = new Array();
		for (index = 0; index < operator_number; index++)
		{
			this.operators.push({ generator : new Oscillator(440.0,1.0), factor : 1.0});
		}
		this.outputs = new Array();
		this.outputs.push({ operator : this.operators[0].generator, velocity : 1.0 });
	}

	inherits(FMSynthetizer, ISoundGenerator);

	FMSynthetizer.prototype.setFrequency = function(frequency_)
	{
		for (index = 0; index < this.operators.length; index++)
		{
			this.operators[index].generator.setFrequency(frequency_  * this.operators[index].factor);
		}
	}

	FMSynthetizer.prototype.unplugall = function()
	{
		this.outputs = new Array();
		for (index = 0; index < this.operators.length; index++)
		{
			this.operators[index].generator.frequency.unmodulate();
			this.operators[index].generator.amplitude.unmodulate();			
		}		
	}

	FMSynthetizer.prototype.plugoutput = function(operator_index, velocity)
	{
		this.outputs.push({ 
			operator : this.operators[operator_index].generator
			, velocity : velocity 
		});		
	}

	FMSynthetizer.prototype.process = function(t)
	{
		if (this.isStarted == false) { return 0.0; }
		var value = 0.0;
		var velocity_total = 0.0;
		for (index = 0; index < this.outputs.length; index++)
		{			
			value += this.outputs[index].operator.process(t) * this.outputs[index].velocity;
			velocity_total += this.outputs[index].velocity;
		}
		return value/velocity_total;
	}

	return FMSynthetizer;
})();

module.exports = FMSynthetizer;