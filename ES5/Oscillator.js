var ISoundGenerator = require('./ISoundGenerator.js');
var Modulator = require('./Modulator.js');
const inherits = require('inherits');

const _2PI = Math.PI * 2.0;

var Oscillator = (function()
{
	function Oscillator(frequency_, amplitude_)
	{
		ISoundGenerator.call(this);
		this.frequency = new Modulator(frequency_,0.0,0.0);	
		this.amplitude = new Modulator(amplitude_,0.0,0.0);
	}

	inherits(Oscillator,ISoundGenerator);

	Oscillator.prototype.process = function(t)
	{
		if (this.isStarted == false) { return 0.0; }
		var frequency = this.frequency.process(t);
		var amplitude = this.amplitude.process(t);
		var phase = t * frequency;
	    return Math.sin(_2PI * phase)  * amplitude ;
	}

	Oscillator.prototype.setFrequency = function(frequency_)
	{
		this.frequency.main = frequency_;
	}

	Oscillator.prototype.setAmplitude = function(amplitude_)
	{
		this.amplitude.main = amplitude_;
	}
	return Oscillator;
})();

module.exports = Oscillator;