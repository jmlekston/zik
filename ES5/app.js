var Throttle = require('throttle');
var Speaker = require('speaker');
var FMSynthetizer = require('./FMSynthetizer.js');
var FMSynthetizerFactory = require('./FMSynthetizerFactory.js');
var SoundEngine = require('./SoundEngine.js');
var MMLPlayer = require('./MMLPlayer.js');

var organ = FMSynthetizerFactory.CreateSimpleOrgan();
var player = new MMLPlayer();
player.voice = organ;
player.setScore("t200 o4 c c c d e2 d2 c e d d c2");

var sound_engine = new SoundEngine();

sound_engine.generator = function(t) 
{ 
	return player.process(t);
}

var throttle = new Throttle(sound_engine.rate*8); //
var speaker = new Speaker({
  channels: 1,          // 2 channels 
  bitDepth: 16,         // 16-bit samples 
  sampleRate: 44100     // 44,100 Hz sample rate 
});
sound_engine.pipe(throttle).pipe(speaker);