var FMSynthetizer = require('./FMSynthetizer.js');

var FMSynthetizerFactory = {};

FMSynthetizerFactory.CreateSimpleOrgan = function()
{
	var organ = new FMSynthetizer(3);
	organ.operators[0].factor = 1.0;
	organ.operators[1].factor = 2.0;
	organ.operators[2].factor = 0.5;

	organ.setFrequency(440.0);
	organ.unplugall();
	organ.plugoutput(0,1.0);
	organ.plugoutput(1,0.5);
	organ.plugoutput(2,0.4);	

	return organ;
}

module.exports = FMSynthetizerFactory;