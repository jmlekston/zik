var NoteTranslator = {};
NoteTranslator.Frequencies =  
[32.70,34.65,36.71,38.89,41.20,43.65,46.25,49.00,51.91,55.00,58.27,61.74];
NoteTranslator.ToFrequency = function(note_number)
{
	var note_index  = note_number % 12;
	var octave = Math.floor(note_number / 12);
	return NoteTranslator.Frequencies[note_index] * Math.pow(2,(octave - 1)); 
}

module.exports = NoteTranslator;