var IProcessor = require('./IProcessor.js');
var Constant = require('./Constant.js');
const inherits = require('inherits');

var Modulator = (function()
{
	function Modulator(main_, factor_, modulation_) 
	{
		IProcessor.call(this);
		this.main = main_;
		this.factor = new Constant(factor_);
		this.modulation = new Constant(modulation_);
	}

	inherits(Modulator, IProcessor);
	Modulator.prototype.process = function(t)
	{
		return this.main + this.factor.process(t) * this.modulation.process(t);
	}

	Modulator.prototype.unmodulate = function()
	{
		this.factor = new Constant(0.0);
		this.modulation = new Constant(0.0);
	}
	return Modulator;
})();

module.exports = Modulator;