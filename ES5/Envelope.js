var Envelope = (function()
{
	function Envelope()
	{
		this.A = 1.0;
		this.S = 0.7;
		this.a = 0.05;
		this.d = 0.05;
		this.s = 0.6;
		this.r = 0.1;
		this.ti = 0.0;

		this.input = null;
	}

	Envelope.prototype.length = function()
	{
		return this.a + this.d + this.s + this.r;
	}

	Envelope.prototype.setLength = function(new_length)
	{
		this.s = new_length - (this.a + this.d + this.r);
		this.s = (this.s >= 0.0)?this.s:0.0; 
	}

	Envelope.prototype.setStartTime = function(start_time)
	{
		this.ti = start_time; 
	}

	Envelope.prototype.compute = function(t)
	{
		var te = t - (this.ti + this.r + this.s + this.d + this.a);
		if (te > 0.0) { return 0.0; }
		te = te + this.r;
		if (te > 0.0) { return (-this.S/this.r * te + this.S); }
		te = te + this.s;
		if (te > 0.0) { return (this.S); }
		te = te + this.d;
		if (te > 0.0) { return ( (this.S - this.A)/this.d * te + this.A); }
		te = te + this.a;
		if (te > 0.0) { return (this.A/this.a * te); }		
	}

	Envelope.prototype.hasEnded = function(t)
	{
		var te = t - (this.ti + this.r + this.s + this.d + this.a);
		return (te > 0.0)?true:false;  
	}

	Envelope.prototype.process =  function(t)
	{
		var coef = this.compute(t);
		if (this.input) 
		{
			return  this.input.process(t) * coef;
		}
		return coef;
	}
	return Envelope;
})(); 

module.exports = Envelope;